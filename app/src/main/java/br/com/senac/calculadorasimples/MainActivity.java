package br.com.senac.calculadorasimples;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView txtDisplay ;
    private EditText txtOperando1;
    private EditText txtOperando2;
    private Button btnSoma;
    private Button btnSubtracao;
    private Button btnMultiplicacao;
    private Button btnDivisao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        txtDisplay = findViewById(R.id.display);
        txtOperando1 = findViewById(R.id.operando1);
        txtOperando2 = findViewById(R.id.operando2);
        btnSoma = findViewById(R.id.btnSoma);
        btnSubtracao = findViewById(R.id.btnSubtracao);
        btnMultiplicacao = findViewById(R.id.btnMultiplicacao);
        btnDivisao = findViewById(R.id.btnDivisao);







        btnSoma.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                double operando1 = Double.parseDouble(txtOperando1.getText().toString());
                double operando2 = Double.parseDouble(txtOperando2.getText().toString());
                double resultado = operando1 + operando2;

                txtDisplay.setText(String.valueOf(resultado));


            }
        });





    }
}
